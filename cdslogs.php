<?php
/**
 * @package     Joomla.Plugin
 * @subpackage  Fabrik.form.cdslogs
 * @copyright   Copyright (C) 2005-2016  Media A-Team, Inc. - All rights reserved.
 * @license     GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

// No direct access
defined('_JEXEC') or die('Restricted access');

use Joomla\Utilities\ArrayHelper;

// Require the abstract plugin class
require_once COM_FABRIK_FRONTEND . '/models/plugin-form.php';

/**
 * Cdslog form submissions
 *
 * @package     Joomla.Plugin
 * @subpackage  Fabrik.form.cdslogs
 * @since       3.0
 */
class PlgFabrik_FormCdslogs extends PlgFabrik_Form
{
	/**
	 * Run when the form loads
	 *
	 * @return  void
	 */
	public function onLoad()
	{
		$params    = $this->getParams();
		$formModel = $this->getModel();
		$view      = $this->app->input->get('view', 'form');

		if ((!$formModel->isEditable() || $view == 'details') && ($params->get('cdslog_details') != '0')) {
			$this->cdslog('form.load.details');
		} elseif ($formModel->isEditable() && ($params->get('cdslog_form_load') != '0')) {
			$this->cdslog('form.load.form');
		}

		return true;
	}

	/**
	 * Get message type
	 *
	 * @param   string $rowId row reference
	 *
	 * @return  string
	 */
	protected function getMessageType($rowId)
	{
		$input = $this->app->input;

		if ($input->get('view') == 'details') {
			return 'form.details';
		}

		if ($rowId == '') {
			return 'form.add';
		} else {
			return 'form.edit';
		}
	}

	/**
	 * Run right at the end of the form processing
	 * form needs to be set to record in database for this to hook to be called
	 *
	 * @return    bool
	 */
	public function onAfterProcess()
	{
		$formModel = $this->getModel();
		$type      = empty($formModel->origRowId) ? 'form.submit.add' : 'form.submit.edit';

		return $this->cdslog($type);
	}

	/**
	 * Get new data
	 *
	 * @return  array
	 */
	protected function getNewData()
	{
		$input     = $this->app->input;
		$formModel = $this->getModel();
		$listModel = $formModel->getListModel();
		$fabrikDb  = $listModel->getDb();
		$item 	   = $listModel->getTable();
		$rowId     = $input->get('rowid', '', 'string');
		// $sql       = $formModel->buildQuery();
		// $fabrikDb->setQuery($sql);

		// return $fabrikDb->loadObjectList();
		// don't want buildQuery(), because of the list prefilters - is_customer prefilter cause empty result if lead set to customer
		$sql 		   = $listModel->buildQuerySelect('form');
		$sql 		   .= $listModel->buildQueryJoin();
		$sql 		   .= ' WHERE ' . $item->db_primary_key . ' = ' . $fabrikDb->quote($rowId);
		$fabrikDb->setQuery($sql);
		return $fabrikDb->loadObjectList();
	}

	/**
	 * Perform cdslog
	 *
	 * @param   string $messageType message type
	 *
	 * @return    bool
	 */
	protected function cdslog($messageType)
	{
		$params        = $this->getParams();
		$formModel     = $this->getModel();
		$listModel 	   = $formModel->getListModel();
		$fabrikDb  	   = $listModel->getDb();
		$input         = $this->app->input;
		$rowId         = $input->get('rowid', '', 'string');
		$loading       = strstr($messageType, 'form.load');
		$userId        = $this->user->get('id');
		$tableid = $formModel->getListModel()->getId();
		$form = $formModel->getForm();
		$formid = $form->id;

		if ($params->get('record_in') == '') {
			$rdb = '#__fabrik_cdslog';
		} else {
			$db_suff = $params->get('record_in');
			$this->_db
				->setQuery(
					"SELECT " . $this->_db->qn('db_table_name') . " FROM " . $this->_db->qn('#__fabrik_lists') . " WHERE "
						. $this->_db->qn('form_id') . " = " . (int)$formid
				);
			$tname = $this->_db->loadResult();
			$rdb   = $tname . $db_suff;
		}

		// if a table name is specified then it overrides the other options
		if ($params->get('table') != '') {
			$rdb = $params->get('table');
		}

		$this->createLogTable($rdb);
		$this->add_column($rdb, "data_comparison_text", "TEXT NOT NULL");

		if ($loading) {
			// $result_compare = FText::_('COMPARE_DATA_LOADING') . $sep_2compare;
		} else {
			$dataJson = $this->getLogMessage();
			$dataText = $this->getLogMessage('text');
			if ($dataJson) {	
				$query = $fabrikDb->getQuery(true);
				$columns = array('date', 'data_comparison', 'rowid', 'userid', 'tableid', 'formid', 'data_comparison_text');
				$values = array($fabrikDb->quote(date("Y-m-d H:i:s")), $fabrikDb->quote($dataJson), $rowId, $userId, $tableid, $formid, $fabrikDb->quote($dataText));
	
				$query
					->insert($fabrikDb->quoteName($rdb))
					->columns($fabrikDb->quoteName($columns))
					->values(implode(',', $values));
	
				$fabrikDb->setQuery($query);
				$fabrikDb->execute();
			}
		} 

		return true;
	}

	private function getLogMessage($format = "json") {
		$formModel     = $this->getModel();
		$params        = $this->getParams();
		$newData = $this->getNewData();
		$text = '';

		if (!empty($newData)) {
			$origData = $formModel->_origData;
			$c = 0;
			$groups = $formModel->getGroupsHiarachy();
			
			foreach ($groups as $groupModel) {
				$group = $groupModel->getGroup();
				$elementModels = $groupModel->getPublishedElements();
				foreach ($elementModels as $elementModel) {
					$element  = $elementModel->getElement();
					$fullName = $elementModel->getFullName(true, false);
					$element_type = "name";
					if ($params->get('element_type') == 'label') {
						$element_type = "label";
					}

					if ($formModel->isNewRecord()) {
						$dataArray["status"] = 'newrecord';
						$dataArray["data"][] = array("element" => $element->$element_type, "value" => $newData[$c]->$fullName);
					} else if (($newData[$c]->$fullName != $origData[$c]->$fullName) && ($newData[$c]->$fullName != null)) {
						$text .= "Changed `{$element->name}` from '{$origData[0]->$fullName}' to '{$newData[$c]->$fullName}'".PHP_EOL;
						$dataArray["status"] = 'modification';
						$dataArray["data"][] = array("element" => $element->$element_type, "from" => $origData[0]->$fullName, "to" => $newData[$c]->$fullName);
					}
				}
			}
		} else {
			return false;
		}

		$result = '';

		switch ($format) {
			case 'json':
				$result = json_encode($dataArray);
				break;

			case 'text':
				$result = $text;
				break;
			
			default:
				# code...
				break;
		}
		return $result;
	}

	private function createLogTable($table) {
		$create_custom_table = "CREATE TABLE IF NOT EXISTS `{$table}` (
			`id` int(11) NOT NULL auto_increment,
			`date` datetime NOT NULL,
			`data_comparison` text NOT NULL,
			`rowid` int(11) NOT NULL default '0',
			`userid` int(11) NOT NULL default '0',
			`tableid` int(11) NOT NULL default '0',
			`formid` int(11) NOT NULL default '0',
			PRIMARY KEY  (`id`)
		  ) ENGINE=MyISAM  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;";
		$this->_db->setQuery($create_custom_table);
		$this->_db->execute();
	}

	function column_exists($table, $column)
	{
		$fields = $this->_db->getTableColumns($table);
			
		if ($fields === null)
			return false;
			
		if (array_key_exists($column,$fields))
			return true;
		else
			return false;
	}

	//-------------------------------------------------------------------------------
	// Add a column if it doesn't exist (the table must exist)
	//
	function add_column($table, $column, $details)
	{
		if ($this->column_exists($table, $column))
			return;
		$query = 'ALTER TABLE `'.$table.'` ADD `'.$column.'` '.$details;
		$this->_db->setQuery($query);
		return $this->_db->execute();
	}
}
